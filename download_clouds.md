# Clouds
The included script `download_clouds.pl` was found at [joffie.swal.org/xplanet/](http://joffie.swal.org/xplanet/).

    wget http://joffie.swal.org/xplanet/scripts/download_clouds.pl
    hg diff

The script has been modified to save the cloud file in the images directory rather than the home directory.
Invalid and unrealiable sources have also been removed out, leaving essentially only two valid sources.
