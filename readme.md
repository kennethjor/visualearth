Visualearth is a script which will generate an image of the world as it currently looks from space.
OK, it's not completely accurate, but it's good enough for a dynamic desktop wallpaper.
Initially inspired by [World Sunlight Map by die.net](http://www.die.net/earth/).

# Requirements
You will need `mercurial`, `curl`, `imagemagick`, `xplanet`, and `perl`.
If you're using a system with `apt` as the package manager, such a Debian or Ubuntu, the following command should suffice:

    :::text
    sudo apt-get install mercurial curl imagemagick xplanet perl libwww-perl

# Installation
First you need to check out the repo.
Navigate to wherever you want to repo to be.

    :::text
    hg clone ssh://hg@bitbucket.org/kennethjor/visualearth
    cd visualearth

In order to get it all working, simply run `./initialize.sh`, which will download all the appropriate images used.
You only need to do this once, and it will fetch about 22 MiB of data.

# Usage
To generate the image, run `./generate.sh`.
This will generate the final image and save it to `render/earth.jpg`.
You can use this image as your desktop background or whatever else you can think of.

# Automation
The generated image is based on current solar position, cloud data, and the month.
This means the image will be different depending on when you generate it.
To set up automatic generation every 5 minutes, simply add an entry like this to your crontab, chaging username and path:

    :::text
    */5  *    * * *   yourusername    cd /path/to/visualearth/ && ./generate.sh

If you're using it as a desktop wallpaper, make sure your OS updates the image on change.
For my personal computer, I have it set up as a slideshow taken from the `render` folder.

# Advanced Usage
generate.sh accepts the following options:

    :::text
        --save
            Causes the script to make a timestamped copy of the generated image
        --size=###x###
            Sets the final size of the generated image

# Clouds
The clouds fetched by `download_clouds.pl` are updated once per day.
For more real-time clouds, you should subscribe to [XplanetClouds](http://xplanetclouds.com/).
Currently my script do not support this out of the box, as I don't have a subscription.
However, it would not be hard to change the cloud script to support it.
Merge requests welcome.

# License
[MIT](http://opensource.org/licenses/MIT).

# Related Projects

* [planetdesk](https://github.com/kevinr/planetdesk) seems to be a project very similar to this.
I have not tried it though, but it may be relevant to your interests.
