#!/bin/bash

set -e

# requires imagemagick and ffmpeg/avconv

size=1280x720
framerate=12

tmpdir=/tmp/visualearth_`date -u +%F_%T`
output=timelapse/timelapse_`date -u +%F_%T`.mp4

mkdir -p $tmpdir
mkdir -p `dirname $output`

echo ==== Processing files ...
i=0
for f in render/earth_*;
do
	name=`basename $f`
	out=$tmpdir/img_`printf "%03d" $i`.jpg
	echo $name
	convert $f \
		-resize $size\! \
		-fill black \
		-draw "rectangle 5,5,187,25" \
		-fill white \
		-annotate +10+20 "$name" \
		$out
	((i=$i+1))
done

echo ==== Encoding video ...
avconv -i $tmpdir/img_%03d.jpg -codec:v libx264 -q 0 -r $framerate $output

rm -R $tmpdir
