#!/usr/bin/env bash
set -e

if [ ! -d "images" ]; then
	mkdir images
fi

download() {
	local note="$1"
	local url="$2"
	local file="$3"
	echo -n "$note - $file"
	if [ -f "$file" ]; then
		echo " already downloaded, skipping"
	else
		echo
		curl --progress-bar --compressed -o "$file" "$url"
	fi
}

echo === Downloading NASA Blue Marble Next Generation
# http://visibleearth.nasa.gov/view_cat.php?categoryID=1484
download " 1/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73938/world.200401.3x5400x2700.jpg" "images/earth_day_01.jpg"
download " 2/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73967/world.200402.3x5400x2700.jpg" "images/earth_day_02.jpg"
download " 3/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73992/world.200403.3x5400x2700.jpg" "images/earth_day_03.jpg"
download " 4/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74017/world.200404.3x5400x2700.jpg" "images/earth_day_04.jpg"
download " 5/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74042/world.200405.3x5400x2700.jpg" "images/earth_day_05.jpg"
download " 6/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/76000/76487/world.200406.3x5400x2700.jpg" "images/earth_day_06.jpg"
download " 7/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74092/world.200407.3x5400x2700.jpg" "images/earth_day_07.jpg"
download " 8/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74117/world.200408.3x5400x2700.jpg" "images/earth_day_08.jpg"
download " 9/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74142/world.200409.3x5400x2700.jpg" "images/earth_day_09.jpg"
download "10/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74167/world.200410.3x5400x2700.jpg" "images/earth_day_10.jpg"
download "11/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74192/world.200411.3x5400x2700.jpg" "images/earth_day_11.jpg"
download "12/12" "http://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74218/world.200412.3x5400x2700.jpg" "images/earth_day_12.jpg"

echo === Downloading NASA topographical map
# http://visibleearth.nasa.gov/view.php?id=73934
download "1/1" "http://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73934/srtm_ramp2.world.5400x2700.jpg" "images/earth_topo.jpg"

echo === Downloading NASA earth lights
# http://visibleearth.nasa.gov/view.php?id=55167
download "1/1" "http://eoimages.gsfc.nasa.gov/images/imagerecords/55000/55167/earth_lights_lrg.jpg" "images/earth_night.jpg"

echo "Done"
